from django.contrib import admin
from .models import Job, Candidate, Match, Event, Email


admin.site.register(Job)
admin.site.register(Candidate)
admin.site.register(Match)
admin.site.register(Event)
admin.site.register(Email)