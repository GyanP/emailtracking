from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Job, Candidate, Match, Event, Email
from .serializers import (
    JobSerializer,
    CandidateSerializer,
    MatchSerializer,
    EventSerializer
)
from datetime import datetime


@api_view(['GET', 'POST'])
def job_api_view(request):
    
    if request.method == 'GET': # user requesting data 
        snippets = Job.objects.all()
        serializer = JobSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST': # user posting data
        serializer = JobSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def candidate_api_view(request):
    
    if request.method == 'GET': # user requesting data 
        snippets = Candidate.objects.all()
        serializer = CandidateSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST': # user posting data
        serializer = CandidateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def match_api_view(request):
    
    if request.method == 'GET': # user requesting data 
        snippets = Match.objects.all()
        serializer = MatchSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST': # user posting data
        serializer = MatchSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def event_api_view(request):
    
    if request.method == 'POST': # user posting data
        type = request.data.get("type", None)
        if type:
            type = str(type.split('.')[1])
        timestamp = request.data.get("date", None)
        date = datetime.fromtimestamp(timestamp)
        request.data.update({"type": type, "date": date})
        serializer = EventSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def filter_events_api_view(request):
    
    if request.method == 'GET': # user requesting data 
        get_job = request.query_params.get('job', None)
        get_candidate = request.query_params.get('candidate', None)
        get_start_date = request.query_params.get('date_from', None)
        get_end_date = request.query_params.get('date_to', None)
        get_match = None

        if get_job:
            get_match = Match.objects.filter(job=get_job)

        if get_candidate:
            get_match = Match.objects.filter(job=get_candidate)

        if get_start_date and get_end_date:
            date_now = datetime.today()
            format = "%Y-%m-%d"
            startdate = datetime.strptime(get_start_date, format)
            enddate = datetime.strptime(get_end_date, format)
            if enddate > date_now:
                return Response("Date Cannot be in the future", status=status.HTTP_404_NOT_FOUND)        
            snippets = Event.objects.filter(date__range=[startdate, enddate])
            serializer = EventSerializer(snippets, many=True)
            return Response(serializer.data)

        if get_match:
            get_email=Email.objects.filter(match=get_match.first())
            snippets = Event.objects.filter(id__in=get_email.values_list('id'))
            serializer = EventSerializer(snippets, many=True)
            return Response(serializer.data)
        return Response(status=status.HTTP_404_NOT_FOUND)