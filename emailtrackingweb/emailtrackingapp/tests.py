import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from emailtrackingapp.models import Candidate, Job, Email, Event, Match


# initialize the APIClient app
client = Client()


class JobCreateAPI(TestCase):
    """ Test for inserting a new job """

    def setUp(self):
        self.valid_payload = {
            'title': 'Developer',
            'state': 'closed',
        }
        self.invalid_payload = {
            'state': 'closed',
        }

    def test_create_valid_job(self):
        response = client.post(reverse('emailtrackingapp:job'),
        data=json.dumps(self.valid_payload),
        content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_job(self):
        response = client.post(
            reverse('emailtrackingapp:job'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class CandidateCreateAPI(TestCase):
    """ Test for inserting a new Candidate """

    def setUp(self):
        self.valid_payload = {
            'name': 'test',
            'email': 'test@gmail.com',
        }
        self.invalid_payload = {
            'email': 'test1@gmail.com',
        }

    def test_create_valid_candidate(self):
        response = client.post(reverse('emailtrackingapp:candidate'),
        data=json.dumps(self.valid_payload),
        content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_candidate(self):
        response = client.post(
            reverse('emailtrackingapp:candidate'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class MatchCreateAPI(TestCase):
    """ Test for create Match for Job and Candidate """

    def setUp(self):
        job = Job.objects.create(title='Developer',state='closed')
        candidate = Candidate.objects.create(name='test',email='test@gmail.com')
        
        self.valid_payload = {
            "job": job.id,
            "candidate":candidate.id,
            "state": "in_progress",
            "interest": "interested"
        }
        self.invalid_payload = {
            "state": "in_progress",
            "interest": "interested"
        }

    def test_create_valid_match(self):
        response = client.post(
            reverse('emailtrackingapp:match'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_match(self):
        response = client.post(
            reverse('emailtrackingapp:match'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)



class EventCreateAPI(TestCase):
    """
        Test for create Events.
        and Filter events based on job, candidate and date(time range)
    """
    # Note I'm testing Filter events in the below test too.

    def setUp(self):
        self.job1 = Job.objects.create(title='Developer1',state='open')
        self.candidate1 = Candidate.objects.create(name='test1',email='test1@gmail.com')
        match1 = Match.objects.create(
            job=self.job1,
            candidate=self.candidate1,
            state="in_progress",
            interest="interested"
        )

        self.job2 = Job.objects.create(title='Developer2',state='open')
        self.candidate2 = Candidate.objects.create(name='test2',email='test2@gmail.com')
        match2 = Match.objects.create(
            job=self.job2,
            candidate=self.candidate2,
            state="in_progress",
            interest="not_interested"
        )

        self.job3 = Job.objects.create(title='Developer3',state='open')
        self.candidate3 = Candidate.objects.create(name='test3',email='test3@gmail.com')
        match3 = Match.objects.create(
            job=self.job3,
            candidate=self.candidate3,
            state="in_progress",
            interest="interested"
        )

        self.job4 = Job.objects.create(title='Developer4',state='open')
        self.candidate4 = Candidate.objects.create(name='test4',email='test4@gmail.com')
        match4 = Match.objects.create(
            job=self.job4,
            candidate=self.candidate4,
            state="in_progress",
            interest="interested"
        )

        self.job5 = Job.objects.create(title='Developer5',state='open')
        self.candidate5 = Candidate.objects.create(name='test5',email='test5@gmail.com')
        match5 = Match.objects.create(
            job=self.job5,
            candidate=self.candidate5,
            state="in_progress",
            interest="interested"
        )

        self.job6 = Job.objects.create(title='Developer6',state='open')
        self.candidate6 = Candidate.objects.create(name='test6',email='test6@gmail.com')
        match6 = Match.objects.create(
            job=self.job6,
            candidate=self.candidate6,
            state="in_progress",
            interest="interested"
        )

        self.valid_events_list = [
            {
                "date": 1654244726,
                "type": "message.created",
                "metadata": {
                    "message_id": "cjld2cjxh0000qzrmn831i7rn",
                    "match_id": match1.id
                }
            },
            {
                "date": 1654244726,
                "type": "message.replied",
                "metadata": {
                    "message_id": "cjld2cjxh0000qzrmn831i7rn",
                    "match_id": match2.id
                }
            },
            {
                "date": 1654244726,
                "type": "message.bounced",
                "metadata": {
                    "message_id": "cjld2cyuq0000t3rmniod1foy",
                    "match_id": match3.id
                }
            },
            {
                "date": 1654158326,
                "type": "message.created",
                "metadata": {
                    "message_id": "cjld2cjxh0000qzrmn831i7rn",
                    "match_id": match4.id
                }
            },
            {
                "date": 1654158326,
                "type": "message.replied",
                "metadata": {
                    "message_id": "cjld2cjxh0000qzrmn831i7rn",
                    "match_id": match5.id
                }
            },
            {
                "date": 1654071926,
                "type": "message.bounced",
                "metadata": {
                    "message_id": "cjld2cyuq0000t3rmniod1foy",
                    "match_id": match1.id
                }
            },
            {
                "date": 1654071926,
                "type": "message.created",
                "metadata": {
                    "message_id": "cjld2cjxh0000qzrmn831i7rn",
                    "match_id": match3.id
                }
            },
            {
                "date": 1654071926,
                "type": "message.replied",
                "metadata": {
                    "message_id": "cjld2cjxh0000qzrmn831i7rn",
                    "match_id": match2.id
                }
            },
            {
                "date": 1651652726,
                "type": "message.bounced",
                "metadata": {
                    "message_id": "cjld2cyuq0000t3rmniod1foy",
                    "match_id": match4.id
                }
            }
        ]


        # Assuming Events Coming from Third party APIs are


        self.invalid_events_list = [
            {
                "date": 1654158326,
                "type": "message.replied",
                "metadata": {
                    "message_id": "cjld2cjxh0000qzrmn831i7rn"
                }
            },
            {
                "date": 1654071926,
                "type": "message.bounced",
            },
            {
                "date": 1654071926,
                "type": "message.created",
                "metadata": {
                    "message_id": "cjld2cjxh0000qzrmn831i7rn",
                    "match_id": 7
                }
            },
            {
                "date": 1654071926,
                "type": "message.replied",
                "metadata": {
                    "message_id": "",
                    "match_id": match1.id
                }
            },
            {
                "date": 1651652726,
                "type": "message.bounced",
                "metadata": {
                    "message_id": None,
                    "match_id": match4.id
                }
            },
            {
                "date": 1683188726,
                "type": "message.bounced",
                "metadata": {
                    "message_id": "cjld2cjxh0000qzrmn831i7rn",
                    "match_id": match2.id
                }
            }
        ]


    def test_create_valid_event(self):
        """
            Test event comming from Third party APIs
            and create Email and Events based on it.
        """
        for event in self.valid_events_list:
            response = client.post(
                reverse('emailtrackingapp:event'),
                data=json.dumps(event),
                content_type='application/json'
            )
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    def test_create_invalid_event(self):
        """
            Test Invalid event comming from Third party APIs
            and create Email and Events based on it.
        """
        for invalid_event in self.invalid_events_list:
            response = client.post(
                reverse('emailtrackingapp:event'),
                data=json.dumps(invalid_event),
                content_type='application/json'
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Filter Events API tests
    def test_filter_events_based_on_valid_job(self):
        """
            Test filter events based on valid job.
        """
        self.test_create_valid_event()
        response = client.get('/api/service/get_events?job='+str(self.job1.id), follow = True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_filter_events_based_on_invalid_job(self):
        """
            Test filter events based on invalid job.
        """
        self.test_create_valid_event()
        response = client.get('/api/service/get_events?job='+str(12), follow = True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    def test_filter_events_based_on_valid_candidate(self):
        """
            Test filter events based on candidate.
        """
        self.test_create_valid_event()
        response = client.get('/api/service/get_events?candidate='+str(self.candidate3.id), follow = True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_filter_events_based_on_invalid_candidate(self):
        """
            Test filter events based on invalid candidate.
        """
        self.test_create_valid_event()
        response = client.get('/api/service/get_events?candidate='+str(10), follow = True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_filter_events_based_on_valid_date_range(self):
        """
            Test filter events based on valid_date (given time range).
        """
        self.test_create_valid_event()
        response = client.get('/api/service/get_events?date_from=2017-05-29&date_to=2022-06-02', follow = True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_filter_events_based_on_invalid_date_range(self):
        """
            Test filter events based on invalid date (given time range).
        """
        self.test_create_valid_event()
        response = client.get('/api/service/get_events?date_from=2017-05-29&date_to=2023-06-02', follow = True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
