from rest_framework import serializers
from .models import Job, Candidate, Match, Email, Event
from datetime import datetime


class JobSerializer(serializers.ModelSerializer):
    """
    JobSerializer
        serializer to get and create Job
    """
    class Meta:
        model = Job
        fields = (
            'id',
            'title',
            'state',
            'date'
        )
        read_only_fields = ('id', 'date')


class CandidateSerializer(serializers.ModelSerializer):
    """
    CandidateSerializer
        serializer to get and create Candidate
    """
    class Meta:
        model = Candidate
        fields = (
            'id',
            'name',
            'email',
            'date'
        )
        read_only_fields = ('id', 'date')


class MatchSerializer(serializers.ModelSerializer):
    """
    MatchSerializer
        serializer to create Job Match
    """
    class Meta:
        model = Match
        fields = (
            'id',
            'job',
            'candidate',
            'state',
            'interest'
        )
        read_only_fields = ('id',)


class EventSerializer(serializers.ModelSerializer):
    """
    EventSerializer
        serializer to create Event
    """

    def validate_metadata(self, metadata):
        """
        Check that the message_id is in the metadata and validate it.
        """
        if 'message_id' not in metadata:
            raise serializers.ValidationError("Invalid metadata")
        elif metadata['message_id'] == "" or metadata['message_id'] == None:
            raise serializers.ValidationError("Message Id cannot black")    
        return metadata

    def validate_date(self, date):
        """
        Check that the date is not in future.
        """
        
        now = datetime.now()
        date=datetime.fromisoformat(str(date))
        if date.isoformat() > now.isoformat():
            raise serializers.ValidationError("Date cannot be in future")
        return date
    
    def create(self, validated_data):
        # create events and email
        type = validated_data["type"]
        date = validated_data["date"]
        metadata = validated_data["metadata"]
        event = super(EventSerializer, self).create(validated_data)

        match_id = ''
        email_status = ''

        if 'match_id' in metadata:
            match_id = metadata['match_id']
        if type == 'bounced':
            email_status = 'bounced'
        elif type == 'replied':
            email_status = 'responded'
            
        if match_id and Match.objects.filter(id=match_id).exists():
            Email.objects.create(
                date=date,
                status=email_status,
                event=event,
                match=Match.objects.filter(id=match_id).first()
            )
        else:
            raise serializers.ValidationError("Match Id Not Found")
        
        return event

    class Meta:
        model = Event
        fields = (
            'id',
            'date',
            'type',
            'metadata'
        )
        read_only_fields = ('id',)
