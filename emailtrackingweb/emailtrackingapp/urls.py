from django.urls import path
from emailtrackingapp import views

app_name = 'emailtrackingapp'
urlpatterns = [
    
    path('api/job',views.job_api_view, name='job'),
    path('api/candidate',views.candidate_api_view, name='candidate'),
    path('api/match',views.match_api_view, name='match'),

    path('api/service/event',views.event_api_view, name='event'),
    path('api/service/get_events',views.filter_events_api_view, name='get_events'),
]