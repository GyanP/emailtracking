from django.db import models

# Create your models here.


class Job(models.Model):

    state_choices = (
        ('open', 'open'),
        ('closed', 'closed'),
    )
    title = models.CharField(max_length=50)
    state = models.CharField(max_length=50, choices=state_choices, default="open")
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Candidate(models.Model):

    name = models.CharField(max_length=50)
    email = models.EmailField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Match(models.Model):

    match_state = (
        ('in_progress', 'in_progress'),
        ('replied', 'replied'),
    )    
    interest = (
        ('interested', 'interested'),
        ('not_interested', 'not_interested'),
    )
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    state = models.CharField(max_length=50,choices=match_state, default="in_progress")
    interest = models.CharField(max_length=50,choices=interest)


class Event(models.Model):
    
    event_type = (
        ("created", "created"),
        ("replied", "replied"),
        ("bounced", "bounced"),
    )
    date = models.DateTimeField()
    type = models.CharField(choices=event_type, max_length=50)
    metadata = models.JSONField()


class Email(models.Model):

    status = (
        ('bounced', 'bounced'),
        ('responded', 'responded'),
    )
    date = models.DateTimeField()
    status = models.CharField(max_length=50,choices=status)
    event = models.ForeignKey(Event, models.CASCADE, related_name='event')
    match = models.ForeignKey(Match, on_delete=models.CASCADE, related_name='match')