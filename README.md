# EmailTracking

## Description
A services that exposes one or more API endpoints to handle events and display statistics about the outreach status at any point in time.

## Installation
Clone repo from gitlab
```
https://gitlab.com/GyanP/emailtracking.git
```
Command:
```
git clone https://gitlab.com/GyanP/emailtracking.git
```

## Run Project
To Run docker and project please make sure you have latest version of docker
and docker-compose installed and use latest version of python3 or latest version.

Run below command for build:

```
sudo docker-compose up --build
```

After above command please Run below command for create migrations:
```
docker exec -it emailtrackingweb_web_1 bash
```
and

```
python manage.py makemigrations
python manage.py migrate
```

To Test Services that i created you can run test cases also run it with APIs.
here i provide the way to run it using Test cases.

After above commads run below command:

```
python manage.py test
```

Here are some service endpoints to test services by creating it:

## Create Job
```
http://localhost:8000/api/job
Request Method: POST
data = {
    "title": "Software Engineer",
    "state": "open"
}
```

## Create Candidate
```
http://localhost:8000/api/candidate
Request Method: POST
{
    "name": "Josh",
    "email": "josh@gmail.com"
}
```

## Create Match
```
http://localhost:8000/api/match
Request Method: POST

{
"job": 1,
"candidate":1,
"state": "progress",
"interest": "interested"
}
```

## Create Events and Email
```
http://localhost:8000/api/service/event

Request Method: POST

{
    "date": 1683188726,
    "type": "message.created",
    "metadata": {
        "message_id": "cjld2cjxh0000qzrmn831i7rn",
        "match_id": 1
    }
}
```

## Filter Events with job
```
http://localhost:8000/api/service/get_events?job=1

Request Method: GET
```

## Filter Events with Candidate
```
http://localhost:8000/api/service/get_events?candidate=1

Request Method: GET
```

## Filter Events with Date(date-range)
```
http://localhost:8000/api/service/get_events?date_from=2016-05-29&date_to=2022-06-01

Request Method: GET
```
